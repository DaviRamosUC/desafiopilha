package program;

import java.util.Scanner;
import java.util.Stack;

public class Program {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Stack<Double> pilha = new Stack<Double>();
		Double conteudo;

		do {
			System.out.print("Informe o valor: ");
			conteudo = sc.nextDouble();
			if (conteudo >=0) {				
				pilha.push(conteudo);
			}
		} while (conteudo >= 0);

		System.out.println("\nOs valores da pilha s�o: ");
		for (Double d : pilha) {
			System.out.println(d);
		}

		System.out.println("Quantidade de ocorrencias de primos seguidos: " + contadorPrimos(pilha));

		sc.close();
	}

	public static Integer contadorPrimos(Stack<Double> p) {
		Integer contador = 0;

		for (int i = 0; i < p.size()-1; i++) {
			if (p.elementAt(i) % 2 != 0 && p.elementAt(i+1) % 2 != 0) {
				contador++;
			}
		}

		return contador;

	}

}
